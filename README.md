# Data lab

## Description

Metadata and link to datasets,

scripts making use of these datasets,

narratives making use of these datasets.

## Notebook maintained while doing this

https://hypothes.is/stream?q=tag:%22p:notebook%2Fdatalab%22%20user:jibe

## Contribute

You are very welcome to contribute! Do as you wish and just [ping me](https://twitter.com/jibe_jeybee)
