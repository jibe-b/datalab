import os
import csv
import matplotlib.pyplot as plt

narratives_dir = "../narrative"

os.system('mkdir tmp')

structure_list = [
"5Z02", 
"6B1K",
"6B38",
"6B3F",
"6BXG",
"6ES9",
"5MH0",
"5MH1",
"5MH2",
"5N1R"
]

for structure in structure_list:
    os.system('cd tmp;wget https://www.rcsb.org/pdb/download/downloadFastaFiles.do?structureIdList=' + structure + '&compressionType=uncompressed')

os.system('ls tmp/')

os.system('cd tmp; cat downloadFastaFiles.do?structureIdList=* | grep -v ">" > all-fasta.txt')

os.system("sed 's_._&\n_g' tmp/all-fasta.txt | sort | uniq -c | awk '{print $1\",\"$2}' > tmp/nucleotides-count.txt")

nucleotides_distribution = csv.read('tmp/nucleotides-count.txt')

plt.plot(nucleotides_distribution[0], nucleotides_distribution[1])

# os.system('rm -rf tmp')
